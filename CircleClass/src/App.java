public class App {
    public static void main(String[] args) throws Exception {
        Circle circle1 = new Circle();
        Circle circle2 = new Circle(3.0);
        /* subtask 3
        System.out.println(circle1);
        System.out.println(circle2);
        */
        System.out.println("Dien tich hinh tron 1: " + circle1.getArea());
        System.out.println("Chu vi hinh tron 1: " + circle1.getCircumference());
        System.out.println("Dien tich hinh tron 2: " + circle2.getArea());
        System.out.println("Chu vi hinh tron 2: " + circle2.getCircumference());
    }
}
