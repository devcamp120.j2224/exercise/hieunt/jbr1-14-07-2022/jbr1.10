public class Circle {
    double radius = 1.0;
    // khởi tạo không tham số
    public Circle() {
    }
    // khởi tạo đủ tham số
    public Circle(double radius) {
        this.radius = radius;
    }
    // getter setter
    public double getRadius() {
        return radius;
    }
    public void setRadius(double radius) {
        this.radius = radius;
    }
    public double getArea() {
        double area = radius * radius * Math.PI;
        return area;
    }
    public double getCircumference() {
        double circumference = radius * 2 * Math.PI;
        return circumference;
    }
    @Override
    public String toString() {
        return "Circle [radius=" + radius + "]";
    } 
}
